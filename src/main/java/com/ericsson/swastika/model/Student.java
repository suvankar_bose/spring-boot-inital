package com.ericsson.swastika.model;

import java.util.Date;

import javax.persistence.*;

@Entity
public class Student {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	
	private String studentName;
	
	private String address;
	
	private String branch;
	
	
	public Student (){ 
		
	}
	
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateOfRegistration;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Date getDateOfRegistration() {
		return dateOfRegistration;
	}

	public void setDateOfRegistration(Date dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", studentName=" + studentName
				+ ", address=" + address + ", branch=" + branch
				+ ", dateOfRegistration=" + dateOfRegistration + "]";
	}
	
	
	
	
	

}
