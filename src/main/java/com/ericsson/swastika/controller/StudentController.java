package com.ericsson.swastika.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ericsson.swastika.model.Student;
import com.ericsson.swastika.service.impl.StudentServiceImpl;


@RestController
@RequestMapping("/student")
public class StudentController {
	
	@Autowired
	StudentServiceImpl studentServiceImpl;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Student> getAllStudents(){
		return studentServiceImpl.findAllStudents();
	}
	
	
	@RequestMapping(method = RequestMethod.POST)
	public Student registerStudent(@RequestBody Student student){
		System.out.println("Register" +student);
		return studentServiceImpl.registerStudent(student);
		
	}
	@RequestMapping(method = RequestMethod.PUT)
	public Student updateStudent(@RequestBody Student student){
		System.out.println("Update " +student);
		return studentServiceImpl.updateStudent(student);
		
	}
	@RequestMapping(method = RequestMethod.DELETE)
	public boolean deleteStudent(@RequestBody Student student){
		System.out.println("deleteStudent" +student);
		return studentServiceImpl.deleteStudent(student);
		
		
	}
	@RequestMapping(value="/{rollnumber}",method = RequestMethod.GET)
	public Student findStudentByRollNumber(@PathVariable String rollnumber){
		Student student = new Student();
		student.setId(Long.parseLong(rollnumber));
		return 	studentServiceImpl.findStudentByRollNumber(student);
	}
	

	
	

}


