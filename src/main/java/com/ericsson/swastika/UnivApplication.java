package com.ericsson.swastika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnivApplication {

	public static void main(String[] args) {
		System.out.print("Swastika Application in main method");
		SpringApplication.run(UnivApplication.class, args);
		
	}
}
