package com.ericsson.swastika.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ericsson.swastika.model.Student;
import com.ericsson.swastika.repositroy.StudentRepository;
import com.ericsson.swastika.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentRepository studentRepositry;

	@Override
	public List<Student> findAllStudentsByName(String name) {

		return studentRepositry.findAllByStudentName(name);
	}

	@Override
	public Student registerStudent(Student student) {
		
		System.out.print("Register Student");
		
		if (student != null && student.getStudentName() != null) {
			student.setDateOfRegistration(new Date());
			student = studentRepositry.save(student);
			
			System.out.print("Register Student after registartion" + student);
			
		}

		return student;
	}

	@Override
	public Student updateStudent(Student student) {
		
		System.out.print("updateStudent Start " + student);
		
		if (student != null && student.getId() != null) {

			Student _student = studentRepositry.findOne(student.getId());
			if (_student != null) {

				if (student.getStudentName() != null) {
					_student.setStudentName(student.getStudentName());
				}
				if (student.getAddress() != null) {
					_student.setAddress(student.getAddress());
				}
				if (student.getBranch() != null) {
					_student.setBranch(student.getBranch());
				}
				if (student.getStudentName() != null) {
					_student.setStudentName(student.getStudentName());
				}

			
				
				
				student = studentRepositry.save(_student);
				
				System.out.print("updateStudent end" + student);
			}

		}
		return student;
	}

	@Override
	public boolean deleteStudent(Student student) {

		boolean retrnFlag = false;

		if (student != null && student.getId() != null) {
			Student _student = studentRepositry.findOne(student.getId());

			if (_student != null) {
				studentRepositry.delete(student);
				retrnFlag = true;
			}

		}

		return retrnFlag;
	}

	@Override
	public Student findStudentByRollNumber(Student student) {

		if (student != null && student.getId() != null) {

			Student _student = studentRepositry.findOne(student.getId());

			return _student;

		}

		return null;
	}

	@Override
	public List<Student> findAllStudents() {
		return studentRepositry.findAll();
	}

}
