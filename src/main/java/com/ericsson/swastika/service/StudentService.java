package com.ericsson.swastika.service;

import java.util.List;

import com.ericsson.swastika.model.Student;

public interface StudentService {
	
	public List<Student> findAllStudentsByName(String name);
	public Student registerStudent(Student student);
	public Student updateStudent(Student student);
	public boolean deleteStudent(Student student);
	public Student findStudentByRollNumber(Student student);
	public List<Student> findAllStudents();
	

}
